<footer class="footer-inner">
	<section class="row">
		<?php if ( is_active_sidebar( 'endereco' ) ) : ?><?php dynamic_sidebar( 'endereco' ); ?><?php endif; ?>
		<?php if ( is_active_sidebar( 'telefone' ) ) : ?><?php dynamic_sidebar( 'telefone' ); ?><?php endif; ?>
		<?php if ( is_active_sidebar( 'produtos' ) ) : ?><?php dynamic_sidebar( 'produtos' ); ?><?php endif; ?>
		<?php if ( is_active_sidebar( 'receitas' ) ) : ?><?php dynamic_sidebar( 'receitas' ); ?><?php endif; ?>
	</section>
	<section class="row copy-inner">
		<div class="linha-footer">
			<hr class="line-divide-footer">
			<img src="<?php bloginfo('template_directory'); ?>/img/flor-footer-condex.png" alt="" class="flor-footer">
		</div>
		<div class="small-12 medium-9 large-9 column">
			<p>Todos os Direitos Reservados 2017® - Desenvolvido por <a href="http://www.settemkt.com.br/">Sette Mkt</a></p>
		</div>
		<div class="small-12 medium-4 large-2 column">
			<p><a href="http://www.condexcomercial.com.br" target="_blank">Condex Comercial</a></p>
		</div>
	</section>
</footer>
<?php wp_footer(); ?>
</body>
</html>

