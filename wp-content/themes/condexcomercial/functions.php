<?php  

// REGISTRO DE MENUS
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

 // POST THUMBNAIL
add_theme_support( 'post-thumbnails');

// REGISTRO DE WIDGETS
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Endereço', 'textdomain' ),
        'id'            => 'endereco',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<div class="small-12 medium-4 column">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="titulo-light">',
        'after_title'   => '</h5>',
    ));

     register_sidebar( array(
        'name'          => __( 'Telefone', 'textdomain' ),
        'id'            => 'telefone',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<div class="small-12 medium-3 column">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="titulo-light">',
        'after_title'   => '</h5>',
    ));

    register_sidebar( array(
        'name'          => __( 'Produtos', 'textdomain' ),
        'id'            => 'produtos',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<div class="small-12 medium-2 column">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="titulo-light">',
        'after_title'   => '</h5>',
    ));

    register_sidebar( array(
        'name'          => __( 'Social Media Top', 'textdomain' ),
        'id'            => 'social-top',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<div class="small-12 large-4 medium-4 column">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ));

    register_sidebar( array(
        'name'          => __( 'Receitas', 'textdomain' ),
        'id'            => 'receitas',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<div class="small-12 large-3 medium-3 column receitas">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="titulo-light">',
        'after_title'   => '</h5>',
    ));
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );


// BREADCRUMB WOOCOMCERCE
// add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
// function jk_woocommerce_breadcrumbs() {
//     return array(
//             'delimiter'   => ' &#47; ',
//             'wrap_before' => '
//             <section class="topo-inner"><div class="row breadcrumb"><h1 class="titulo-main txt-left"></h1>,
//             'wrap_after'  => '</div></section>',
//             'before'      => '',
//             'after'       => '',
//             'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
//         );
// }

/* Remover Tab Avaliações */
add_filter( 'woocommerce_product_tabs', 'cwp_woocommerce_remove_default_tabs' );
function cwp_woocommerce_remove_default_tabs( $tabs ) {
             
      if ( isset( $tabs['reviews'] ) ) {
            unset( $tabs['reviews'] );          
      }
       
      return $tabs;
}

// add_filter( 'single_template', function ( $single_template ) {

//     $parent     = '16'; //Change to your category ID
//     $categories = get_categories( 'child_of=' . $parent );
//     $cat_names  = wp_list_pluck( $categories, 'name' );

//     if ( has_category( 'receitas' ) || has_category( $cat_names ) ) {
//         $single_template = dirname( __FILE__ ) . '/single-receitas.php';
//     }
//     return $single_template;

// }, PHP_INT_MAX, 2 );

// ATIVA O SINGLE CATEGORY -
## CRIE UM ARQUIVO COM O NOME single-category-NOMEDACATEGORIA.PHP
add_filter('single_template', 'check_for_category_single_template');
function check_for_category_single_template( $t )
{
  foreach( (array) get_the_category() as $cat ) 
  { 
    if ( file_exists(TEMPLATEPATH . "/single-category-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-category-{$cat->slug}.php"; 
    if($cat->parent)
    {
      $cat = get_the_category_by_ID( $cat->parent );
      if ( file_exists(TEMPLATEPATH . "/single-category-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-category-{$cat->slug}.php";
    }
  } 
  return $t;
}

// UPLOAD

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category() ){
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if ( $image ) {
            echo '<img src="' . $image . '" alt="" />';
        }
    }
}
