<section class="topo-inner">
	<div class="row">
		<h2 class="titulo-main txt-left"><?php woocommerce_page_title(); ?></h2>
		<p class="sub-titulo white">
			
				<?php if ( ! defined( 'ABSPATH' ) ) {
				exit;
				}
				if ( ! empty( $breadcrumb ) ) {
				echo $wrap_before;
				foreach ( $breadcrumb as $key => $crumb ) {
				echo $before;
				if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
						echo '<a itemprop="item" href="' . esc_url( $crumb[1] ) . '"><span itemprop="name">' . esc_html( $crumb[0] ) .'</span></a>';
						echo '<meta itemprop="position" content="1" />';
				} else {
						echo esc_html( $crumb[0] );
				}
				echo $after;
				if ( sizeof( $breadcrumb ) !== $key + 1 ) {
						echo $delimiter;
				}
				}
				echo $wrap_after;
				} ?>
			
		</p>
	</div>
</section>
<?php
/**
* Shop breadcrumb
*
* This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
		* @see 	    https://docs.woocommerce.com/document/template-structure/
				* @author 		WooThemes
		* @package 	WooCommerce/Templates
* @version     2.3.0
* @see         woocommerce_breadcrumb()
*/