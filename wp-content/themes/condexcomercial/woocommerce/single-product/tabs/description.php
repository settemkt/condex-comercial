<?php
/**
* Description tab
*
* This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
	* @see 	    https://docs.woocommerce.com/document/template-structure/
		* @author 		WooThemes
	* @package 	WooCommerce/Templates
* @version     2.0.0
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post;
	$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Product Description', 'woocommerce' ) ) );
?>
<?php if ( $heading ): ?>


<!-- 
<h2 class="titulo-main txt-center"><?php echo $heading; ?></h2>
<ul class="list-linha-verde">
	<li></li>
	<li><p class="sub-titulo verde">mais informações</p></li>
	<li></li>
</ul>
<?php endif; ?>

<div class="margin-20"></div>

<?php the_content(); ?> -->

<!-- <div class="linha-footer margin-80" >
	<hr class="line-divide-footer">
	<img src="http://127.0.0.1/condexwp.com.br/wp-content/themes/condexcomercial/img/flor-footer-condex.png" alt="" class="flor-footer white">
</div> -->

