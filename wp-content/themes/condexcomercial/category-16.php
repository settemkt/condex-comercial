<?php get_header(); ?>

<section class="topo-inner">
	<div class="row">
		<h1 class="titulo-main txt-left"><?php the_title(); ?></h1>
		<p class="sub-titulo"><?php the_date('d - m - Y' ); ?></p>
		<hr class="linha-sub-titulo">
	</div>
</section>

<article class="row">
	<?php
	// the query to set the posts per page to 3
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;?>
	<!-- the loop -->
	<div class="small-12 medium-8 column">
		<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
		<!-- rest of the loop -->
		<?php the_content(); ?>
		<div class="small-12 medium-4 column">
			<?php the_post_thumbnail('img-receitas' ); ?>
		</div>
		<!-- the title, the content etc.. -->
		<?php endwhile; ?>
		
		<!-- pagination -->
		<?php else : ?>
		<!-- No posts found -->
		<?php endif; ?>
	</article>
<?php get_footer(); ?>