<?php get_header(); ?>


<section class="topo-inner">
	<div class="row">
		<h1 class="titulo-main txt-left">Erro 404</h1>
		<p class="sub-titulo white">Página não encontrada</p>
		<hr class="linha-sub-titulo">
	</div>
</section>

<article class="row mart-botom-60">

<div class="small-12 large-8 medium-8 column">
 <h2>Esta página não foi encontrada</h2>
<p>Pedimos desculpa, mas a página que acessou não está mais disponível! Elá pode ter sido removida ou alterada.</p>
<p>Já verificou na barra de endereço do seu browser de internet se o URL está correto?</p>
<p>Se estiver tudo OK, por que não tentar fazer uma pesquisa em nosso site pelo conteúdo que está procurando?</p>

<p>Se desejar, poderá retornar para a nossa <a href="http://www.condexcomercial.com.br">home page</a></p>
</div>

<div class="small-12 large-4 medium-4 column">
<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-condex-comercial404.png" alt="Logo Condex Comercial" class="img-logo"></a>
</div>
</article>

<?php get_footer(); ?>