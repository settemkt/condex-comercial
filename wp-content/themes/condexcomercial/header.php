<!doctype html>
<html class="no-js" lang="<?php bloginfo('language'); ?>">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title><?php woocommerce_page_title(); ?>  <?php bloginfo('name'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="canonical" href="www.condexcomercial.com.br"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/menu.css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/scripts.js"></script>
		<?php wp_head(); ?>
	</head>
	<body>
		<!-- Google Tag Manager -->
		<script async='async'>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-59L8JWM');</script>
		<!-- End Google Tag Manager -->
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-59L8JWM"
		height="0" width="0" style="display:none;visibility:hidden" async='async'></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<header>
			<section class="container-fluid header-inner">
				<div class="row">
					<div class="small-12 large-4 medium-4 column">
						<p>Entre em contato conosco<br><strong><a href="mailto:contato@condexcomercial.com.br">contato@condexcomercial.com.br</a></strong></p></div>
						<div class="small-6 large-4 medium-4 column">
							<p>Ligue para nós <br><strong>(11) 3228-6048 // (11) 2503-7021</strong></p>
						</div>
						<div class="small-6 large-4 medium-4 column">
							<p>Siga nossa Fan Page <br><strong><a target="_blank" href="https://www.facebook.com/Condex-205595956139941/?fref=ts">Condex Comercial</strong></p>
						</div>
					</section>
					<div class="row margin-40">
						<div class="small-10 small-centered medium-uncentered  medium-4 column">
							<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/condexcomercial-logo.png" alt="Logo Condex Comercial" class="img-logo"></a>
						</div>
						<!-- <div class="small-12 medium-8 column">
							<?php //echo do_shortcode('[aws_search_form]'); ?>
							<a id="touch-menu" class="mobile-menu" href="#"><i class="icon-reorder"></i>Menu</a>
							<?php  //wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'menu' ) );?>
						</div> -->
						<div class="small-12 medium-8 column">
						
						
						
							<?php echo do_shortcode('[aws_search_form]'); ?>
							<?php  wp_nav_menu( array( 'theme_location' => 'header-menu',  'container_class' => '-', 'container_id' => 'cssmenu' ) );?>
							<!--  <ul>
								<li class="active"><a href="#" target="_blank"><span><i class="fa fa-fw fa-home"></i> Home</span></a></li>
								<li class="has-sub"><a href="#"><span><i class="fa fa-fw fa-bars"></i> Menus</span></a>
								<ul>
									<li class="has-sub"><a href="#"><span>Menu 1</span></a>
									<ul>
										<li><a href="#"><span>Menu 1.1</span></a></li>
										<li><a href="#"><span>Menu 1.2</span></a></li>
									</ul>
								</li>
								<li><a href="#"><span>Menu 2</span></a></li>
							</ul>
						</li>
						<li><a href="#"><span><i class="fa fa-fw fa-cog"></i> Settings</span></a></li>
						<li><a href="#"><span><i class="fa fa-fw fa-phone"></i> Contact</span></a></li>
					</ul> -->
				</div>
			</div>
			
		</header>