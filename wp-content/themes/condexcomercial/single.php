<?php

// $post = $wp_query->post;

// if ( in_category( 'receitas' ) ) {
//   include( TEMPLATEPATH.'/single-receitas.php' );
// } 
// else {
//   include( TEMPLATEPATH.'/single.php' );
// }
?>

<?php get_header(); ?>


<section class="topo-inner">
	<div class="row">
		<h1 class="titulo-main txt-left"><?php the_title(); ?></h1>
		<p class="sub-titulo white"><?php the_time('d/m/Y' ); ?>  <?php $category = get_the_category(); if($category[0]){ echo '<a href="'.get_category_link($category[0]->term_id ).'">'.$category[0]->cat_name.'</a>';} ?></p>
		<hr class="linha-sub-titulo">

	</div>
</section>

<article class="row mart-botom-60">
	<?php
	// the query to set the posts per page to 3
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;?>
	<!-- the loop -->
	<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
	<!-- rest of the loop -->
	<?php the_content(); ?>
	<?php the_post_thumbnail(); ?>
	<!-- the title, the content etc.. -->
	<?php endwhile; ?>
	<!-- pagination -->
	<?php else : ?>
	<!-- No posts found -->
	<?php endif; ?>
</article>



<?php get_footer(); ?>